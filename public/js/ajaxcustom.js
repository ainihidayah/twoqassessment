//var url = "http://localhost:1001/productajaxCRUD";
var url = "http://localhost:1001/claim";
    //display modal form for product editing
    $(document).on('click','.open_modal',function(){
        var product_id = $(this).val();
       
        $.get(url + '/' + product_id, function (data) {
            //success data
            console.log(data);
            $('#product_id').val(data.id);
            $('#name').val(data.name);
            $('#details').val(data.details);
            $('#btn-save').val("update");
           // $('#myModal').modal('show');
        }) 
    });
	
	
    //display modal form for creating new product
    $('#btn_add').click(function(){
        $('#btn-save').val("add");
        $('#frmProducts').trigger("reset");
        $('#myModal').modal('show');
    });
	
	
    //delete product and remove it from list
    $(document).on('click','.delete-product',function(){
        var product_id = $(this).val();
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $.ajax({
            type: "DELETE",
            url: url + '/' + product_id,
            success: function (data) {
                console.log(data);
                $("#product" + product_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
	
	
    //create new product / update existing product
    $(".save_button").click(function (e) {
		var url = "http://localhost:1001/claim";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
		//id = .replace ( /[^\d.]/g, '' );
        e.preventDefault(); 
        var formData = { 
            //cldt_date: $('#cldt_date'+1).val(),
            cldt_date:'test',
			id : row_id,
            cldt_description: $('#cldt_description'+id).val(),
            status: $('#status'+id).val(),
            receipt_no: $('#receipt_no'+id).val(),
            amount: $('#amount'+id).val(),
            amount_tax: $('#amount_tax'+id).val(),
            total_price: $('#total_price'+id).val(),
        }
        //used to determine the http verb to use [add=POST], [update=PUT]
       // var state = $('#editForm').val();
        var state = "update";
        var type = "POST"; //for creating new resource
        var itemId = $('#itemId').val();;
        var my_url = url;
        if (state == "update"){
            type = "PUT"; //for updating existing resource
            my_url += '/'+id;
        }
        console.log(formData);
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                var product = '<tr id="product' + data.id + '">'
				+ '<td>' + data.cldt_date + '</td>'
				+ '<td>' + data.description + '</td>'
				+ '<td>' + data.status + '</td>';
                product += '<td><button class="btn btn-warning btn-detail open_modal" value="' + data.id + '">Edit</button>';
                product += ' <button class="btn btn-danger btn-delete delete-product" value="' + data.id + '">Delete</button></td></tr>';
            },
            error: function (data) {
                console.log('Error:', data + 'data.clai_date= '+cldt_date );
            }
        });
    });