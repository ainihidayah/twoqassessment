

        $(document).ready(function() {
           //Kawalam Keselamatan
           var barData = {
               labels: ["01-08", "02-08", "03-08", "04-08", "05-08", "06-08", "07-08"],
               datasets: [{
                       label: "Pelawat Awam",
                       backgroundColor: 'rgba(26, 179, 148, 0.5)',
                       borderColor: "rgba(26,179,148,0.7)",
                       pointBackgroundColor: "rgba(26,179,148,1)",
                       pointBorderColor: "#fff",
                       data: [65, 59, 80, 81, 56, 55, 40]
                   },
                   {
                       label: "Pelawat Kakitangan",
                       backgroundColor: '#f4e16d',
                       pointBorderColor: "#fff",
                       data: [28, 48, 40, 19, 86, 27, 90]
                   }
               ]
           };

           var ctx2 = document.getElementById("barChartPelawatMasuk").getContext("2d");
           new Chart(ctx2, {
               type: 'bar',
               axisX: {
                   title: "Timeline",
                   gridThickness: 2
               },
               axisY: {
                   title: "Record"
               },
               data: barData,
               options: {
                   scales: {
                       xAxes: [{
                           stacked: true
                       }],
                       yAxes: [{
                           stacked: true
                       }]
                   }
               }
           });

           var barData = {
               labels: ["01-08", "02-08", "03-08", "04-08", "05-08", "06-08", "07-08"],
               datasets: [{
                       label: "Pegawai",
                       backgroundColor: 'rgba(26, 179, 148, 0.5)',
                       borderColor: "rgba(26,179,148,0.7)",
                       pointBackgroundColor: "rgba(26,179,148,1)",
                       pointBorderColor: "#fff",
                       data: [4, 1, 2, 10, 4, 12, 20]
                   },
                   {
                       label: "LLP",
                       backgroundColor: '#f4e16d',
                       pointBorderColor: "#fff",
                       data: [28, 48, 40, 19, 30, 27, 10]
                   }
               ]
           };

           var ctx2 = document.getElementById("barChartAnggotaKeluar").getContext("2d");
           new Chart(ctx2, {
               type: 'bar',
               axisX: {
                   title: "Timeline",
                   gridThickness: 2
               },
               axisY: {
                   title: "Record"
               },
               data: barData,
               options: {
                   scales: {
                       xAxes: [{
                           stacked: true
                       }],
                       yAxes: [{
                           stacked: true
                       }]
                   }
               }
           });
        });
